import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

@Pipe({
  name: 'image'
})
export class ImagePipe implements PipeTransform {

  transform(img: string, type: string = 'user'): any {
    let url = `${environment.URL_SERVICES}/images`;
    if (!img) return `${url}/default/johnny-blaze.png`;
    if (img.indexOf('https') >= 0) return img;
    switch (type) {
      case 'user':
        url += `/users/${img}`;
        break;

      case 'hospital':
        url += `/hospitals/${img}`;
        break;

      case 'medic':
        url += `/medics/${img}`;
        break;

      default: url += '/users/default/johnny-blaze.png';
    }
    return url;
  }

}
