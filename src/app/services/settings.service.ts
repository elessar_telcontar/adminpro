import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  settings: Settings = {
    urlTheme: 'assets/css/colors/default.css',
    theme: 'default'
  };

  constructor(
    @Inject(DOCUMENT) private _document,
  ) {
    this.loadSettings();
  }

  saveSettings() {
    localStorage.setItem('user-settings', JSON.stringify(this.settings));
  }

  loadSettings() {
    const userSettings = localStorage.getItem('user-settings');
    if (userSettings) {
      this.settings = JSON.parse(userSettings);
    }
    this.applyTheme(this.settings.theme);
  }

  applyTheme(theme: string) {
    const url = `/assets/css/colors/${theme}.css`;
    // document.getElementById('app-theme').setAttribute('href', url);
    this._document.getElementById('app-theme').setAttribute('href', url);

    this.settings = { urlTheme: url, theme };
    this.saveSettings();
  }

}

interface Settings {
  urlTheme: string;
  theme: string;
}
