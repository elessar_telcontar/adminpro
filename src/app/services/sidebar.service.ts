import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu: any = [
    {
      title: 'Main',
      icon: 'mdi mdi-gauge',
      submenu: [
        { title: 'Dashboard', url: '/dashboard' },
        { title: 'Progress', url: '/progress' },
        { title: 'Graphics1', url: '/graphics1' },
        { title: 'Settings', url: '/account-settings' },
        { title: 'Promises', url: '/promises' },
        { title: 'RXJS', url: '/rxjs' },
      ]
    },
    {
      title: 'Settings',
      icon: 'mdi mdi-folder-lock-open',
      submenu: [
        { title: 'Users', url: '/users' },
        { title: 'Hospitals', url: '/hospitals' },
        { title: 'Medics', url: '/medics' }
      ]
    },
  ];

  constructor() { }
}
