import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { environment } from './../../../environments/environment';

import Swal from 'sweetalert2';

import { User } from '@models/user.model';
import { UploadFileService } from '../upload-file.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: User;
  token: string;

  constructor(
    public http: HttpClient,
    public router: Router,
    public uploadService: UploadFileService
  ) {
    this.loadLocalStorage();
  }

  getHeaders = () => {
    return {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + this.token
      })
    };
  }

  loadLocalStorage() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.user = JSON.parse(localStorage.getItem('user'));
    }
  }

  saveStorage(token: string, user: User) {
    localStorage.setItem('token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.token = token;
    this.user = user;
  }

  createUser(user: User) {
    const url = `${environment.URL_SERVICES}/users`;
    return this.http.post(url, user).pipe(
      map((resp: any) => resp.data)
    );
  }

  login(user: User, rememberme: boolean) {
    if (rememberme) localStorage.setItem('email', user.email);
    else localStorage.removeItem('email');
    const url = `${environment.URL_SERVICES}/login`;
    return this.http.post(url, user).pipe(
      map((resp: any) => {
        this.saveStorage(resp.token, resp.data);
        return true;
      })
    );
  }

  logout() {
    this.user = null;
    this.token = null;
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    // localStorage.removeItem('email');
    this.router.navigateByUrl('/login');
  }

  isLogged() {
    return this.token?.length > 0;
  }

  updateUser(user: User) {
    const url = `${environment.URL_SERVICES}/users/${user._id}`;
    return this.http.put(url, user, this.getHeaders()).pipe(
      map((resp: any) => {
        this.saveStorage(this.token, resp.data);
        Swal.fire({
          icon: 'success',
          title: 'Usuario Actualizado'
        });
        return true;
      })
    );
  }

  cambiarImagen(file: File, id: string) {
    this.uploadService.uploadFile(file, 'users', id)
      .then((resp: any) => {
        this.user.img = resp.data.img;
        Swal.fire({
          icon: 'success',
          title: 'Imagen de usuario actualizada'
        });
        this.saveStorage(this.token, this.user);
      }).catch(console.log);
  }

  cargarUsuarios = (skip: number = 0) => {
    const url = `${environment.URL_SERVICES}/users/?skip=${skip}`;
    return this.http.get(url, this.getHeaders());
  }

  buscarUsuarios = (termino: string) => {
    if (termino.length === 0) return this.cargarUsuarios();
    const url = `${environment.URL_SERVICES}/search/users/${termino}`;
    return this.http.get(url, this.getHeaders());
  }

  deleteUser = (id: string) => {
    const url = `${environment.URL_SERVICES}/users/${id}`;
    return this.http.delete(url, this.getHeaders());
  }

}
