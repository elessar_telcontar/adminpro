import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';

import { UserService } from '@services/user/user.service';

import { User } from '@models/user.model';

declare function init_plugins();

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['login-register.component.css']
})
export class RegisterComponent implements OnInit {



  frmRegister: FormGroup;

  constructor(
    public userService: UserService,
    private router: Router
  ) { }

  ngOnInit(): void {
    init_plugins();
    this.frmRegister = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required]),
      repeatPassword: new FormControl(null, [Validators.required]),
      conditions: new FormControl(false)
    }, { validators: this.passwordEquals('password', 'repeatPassword') });
    this.frmRegister.setValue({
      name: 'Test',
      email: 'test@test.com',
      password: '123',
      repeatPassword: '1234',
      conditions: true
    });
  }

  passwordEquals(field0: string, field1: string) {
    return (group: FormGroup) => {
      const pass0 = group.controls[field0].value;
      const pass1 = group.controls[field1].value;
      if (pass0 === pass1) return null;
      return {
        passwordEquals: true
      };
    };
  }

  register() {
    if (this.frmRegister.invalid) return;
    if (!this.frmRegister.value.conditions) Swal.fire('Importante', 'Debe aceptar las condiciones', 'warning');
    const user = new User(
      this.frmRegister.value.name,
      this.frmRegister.value.email,
      this.frmRegister.value.password,
    );
    this.userService.createUser(user).subscribe(resp => {
      this.router.navigateByUrl('/login');
      Swal.fire({
        title: 'Correcto',
        icon: 'success',
        text: `Usuario ${resp.email} creado`
      });
    });
  }

}
