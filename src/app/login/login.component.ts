import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { User } from '@models/user.model';
import { UserService } from '@services/user/user.service';

declare function init_plugins();

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['login-register.component.css']
})
export class LoginComponent implements OnInit {

  rememberme: boolean;
  email: string;

  constructor(
    public router: Router,
    public userService: UserService
  ) { }

  ngOnInit(): void {
    init_plugins();
    if (localStorage.getItem('email')) {
      this.email = localStorage.getItem('email') || '';
      this.rememberme = true;
    }
  }

  login(form: NgForm) {
    if (form.invalid) return;
    const user = new User(null, form.value.email, form.value.password);
    this.userService.login(user, this.rememberme).subscribe(resp => this.router.navigateByUrl('/dashboard'));
  }

  logout() {
    this.userService.logout();
  }

}
