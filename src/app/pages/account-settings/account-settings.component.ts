import { Component, OnInit, Inject } from '@angular/core';

import { SettingsService } from '@services/settings.service';


@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: [
  ],
})
export class AccountSettingsComponent implements OnInit {

  constructor(
    public _settings: SettingsService
  ) { }

  ngOnInit(): void {
    this.putCheck();
  }

  changeTheme(theme: string, link: any) {
    this._settings.applyTheme(theme);
    this.applyCheck(link);
  }

  applyCheck(link: any) {
    const selectors = document.getElementsByClassName('selector');
    Array.from(selectors).forEach(el => el.classList.remove('working'));
    link.classList.add('working');
  }

  putCheck() {
    const selectors = document.getElementsByClassName('selector');
    const theme = this._settings.settings.theme;
    Array.from(selectors).forEach(el => {
      if (theme === el.getAttribute('data-theme')) el.classList.add('working');
    });
  }

}
