import { Component, OnInit } from '@angular/core';

import Swal from 'sweetalert2';

import { UserService } from '@services/user/user.service';
import { ModalUploadService } from '../../components/modal-upload/modal-upload.service';

import { User } from '@models/user.model';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: [
  ],
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  skip = 0;
  limit = 0;
  totalUsers = 0;
  loading: boolean;

  constructor(
    public userService: UserService,
    public modalUploadService: ModalUploadService
  ) { }

  ngOnInit(): void {
    this.cargarUsuarios();
    this.modalUploadService.notificacion.subscribe(resp => this.cargarUsuarios());
  }

  cargarUsuarios = () => {
    this.loading = true;
    this.userService.cargarUsuarios(this.skip).subscribe((res: any) => {
      this.totalUsers = res.data.totalUsers;
      this.users = res.data.users;
      this.loading = false;
    });
  }

  paginateUsers = (value: number) => {
    const skip = this.skip + value;
    if (skip >= this.totalUsers || skip < 0) return;
    this.skip += value;
    this.cargarUsuarios();
  }

  buscarUsuarios = (termino: string) => {
    this.userService.buscarUsuarios(termino).subscribe((res: any) => {
      this.users = res.data.users;
    });
  }

  updateUser = (user: User) => {
    this.userService.updateUser(user).subscribe();
  }

  deleteUser = (user: User) => {
    console.log(user);
    // tslint:disable-next-line: curly
    if (user._id === this.userService.user._id)
      return Swal.fire({ title: 'Usuario no se puede borrar a si mismo', icon: 'error' });
    Swal.fire({
      title: '¿Estás seguro de esta operación?',
      text: 'Esta operación es irreversible!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, Proceder!'
    }).then((result) => {
      if (result.value) {
        this.userService.deleteUser(user._id).subscribe((res: any) => {
          this.cargarUsuarios();
          Swal.fire(
            'OK!',
            `Usuario ${res.data.name} ha sido borrado.`,
            'success'
          );
        });
      }
    });
  }

  showModal(id: string, img: string) {
    this.modalUploadService.showModal('users', id, img);
  }

}
