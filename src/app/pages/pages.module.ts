import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ChartsModule } from 'ng2-charts';

import { PagesRoutingModule } from './pages.routes';

import { PipesModule } from '@pipes/pipes.module';

import { PagesComponent } from './pages.component';
import { SharedModule } from '@shared/shared.module';

import { DashboardComponent } from './dashboard/dashboard.component';
import { Graphics1Component } from './graphics1/graphics1.component';
import { ProgressComponent } from './progress/progress.component';
import { IncrementadorComponent } from '@components/incrementador/incrementador.component';
import { ChartComponent } from '@components/chart/chart.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { PromisesComponent } from './promises/promises.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { UsersComponent } from './users/users.component';
import { ProfileComponent } from './profile/profile.component';
import { ModalUploadComponent } from '@components/modal-upload/modal-upload.component';

@NgModule({
  declarations: [
    PagesComponent,
    DashboardComponent,
    Graphics1Component,
    ProgressComponent,
    IncrementadorComponent,
    ChartComponent,
    AccountSettingsComponent,
    PromisesComponent,
    RxjsComponent,
    UsersComponent,
    ProfileComponent,
    ModalUploadComponent,
  ],
  exports: [
    PagesComponent,
    DashboardComponent,
    Graphics1Component,
    ProgressComponent,
  ],
  imports: [
    PagesRoutingModule, SharedModule,
    FormsModule, CommonModule,
    ChartsModule,
    PipesModule
  ],
})
export class PagesModule { }
