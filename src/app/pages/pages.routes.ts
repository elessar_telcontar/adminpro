import { LoginGuard } from './../guards/login.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';

import { P404Component } from './../shared/p404/p404.component';

import { Graphics1Component } from './graphics1/graphics1.component';
import { ProgressComponent } from './progress/progress.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';
import { ProfileComponent } from './profile/profile.component';
import { PromisesComponent } from './promises/promises.component';
import { RxjsComponent } from './rxjs/rxjs.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {
    path: '', component: PagesComponent,
    canActivate: [LoginGuard],
    children: [
      { path: '', redirectTo: '/dashboard', pathMatch: 'full' }, // Anywhere
      { path: 'dashboard', component: DashboardComponent, data: { title: 'dashboard' } },
      { path: 'progress', component: ProgressComponent, data: { title: 'progress' } },
      { path: 'graphics1', component: Graphics1Component, data: { title: 'graphics' } },
      { path: 'account-settings', component: AccountSettingsComponent, data: { title: 'account' } },
      { path: 'profile', component: ProfileComponent, data: { title: 'profile' } },
      { path: 'promises', component: PromisesComponent, data: { title: 'promises' } },
      { path: 'rxjs', component: RxjsComponent, data: { title: 'rxjs' } },

      // MANTENIMIENTOS
      { path: 'users', component: UsersComponent, data: { title: 'Mantenimiento de Usuarios' } },
    ]
  },
  { path: '**', component: P404Component },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
