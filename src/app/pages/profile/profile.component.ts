import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import Swal from 'sweetalert2';

import { User } from '@models/user.model';

import { UserService } from '@services/user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styles: [
  ],
})
export class ProfileComponent implements OnInit {

  user: User;

  imagenSubir: File;

  imagenTemporal: string;

  constructor(
    public userService: UserService
  ) {
    this.user = this.userService.user;
  }

  ngOnInit(): void {
  }

  save(frmProfile: NgForm) {
    console.log(frmProfile.value);
    const { name, email } = frmProfile.value;
    this.user.name = name;
    this.user.email = email;
    this.userService.updateUser(this.user)
      .subscribe(resp => console.log(resp));
  }

  seleccionImagen(archivo: File) {
    console.log(archivo);
    if (!archivo) return this.imagenSubir = archivo;
    if (archivo.type.indexOf('image') < 0) {
      Swal.fire({ title: 'El archivo seleccionado no es una imagen', icon: 'error' });
      return this.imagenSubir = null;
    }
    this.imagenSubir = archivo;

    const reader = new FileReader();
    // let urlImgTemp = reader.readAsDataURL(archivo);
    reader.readAsDataURL(archivo);
    reader.onloadend = () => this.imagenTemporal = reader.result.toString();

  }

  cambiarImagen() {
    this.userService.cambiarImagen(this.imagenSubir, this.user._id);
  }

}
