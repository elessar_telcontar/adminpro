import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graphics1',
  templateUrl: './graphics1.component.html',
  styles: []
})
export class Graphics1Component implements OnInit {


  graphics: any = {
    graphic_0: {
      labels: ['Con Frijoles', 'Con Natilla', 'Con tocino', 'Con maíz'],
      data:  [24, 30, 46, 10],
      type: 'radar',
      leyenda: 'El pan se come con'
    },
    graphic_1: {
      labels: ['Hombres', 'Mujeres'],
      data:  [4500, 6000],
      type: 'doughnut',
      leyenda: 'Entrevistados'
    },
    graphic_2: {
      labels: ['Si', 'No'],
      data:  [95, 5],
      type: 'pie',
      leyenda: '¿Le dan gases los frijoles?'
    },
    graphic_3: {
      labels: ['No', 'Si'],
      data:  [85, 15],
      type: 'doughnut',
      leyenda: '¿Le importa que le den gases?'
    },
  };

  constructor() { }

  ngOnInit(): void {
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
