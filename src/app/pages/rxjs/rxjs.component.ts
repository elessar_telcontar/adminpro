import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Observer, Subscription } from 'rxjs';
import { retry, map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ],
})
export class RxjsComponent implements OnInit, OnDestroy {

  suscripcion: Subscription;

  constructor() {

    const obs: Observer<any> = {
      next: (data) => console.log(data),
      error: error => console.error(error),
      complete: () => console.log('Completed')
    };

    this.suscripcion = this.returnMyObs().pipe(
      // retry(2)
    ).subscribe(
      obs
    );
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.suscripcion.unsubscribe();
  }

  returnMyObs(): Observable<any> {
    return new Observable(observer => {
      let counter = 0;
      const interval = setInterval(() => {
        counter++;
        const output = {
          value: counter
        };
        observer.next(output);
        /* if (counter === 5) {
          clearInterval(interval);
          observer.complete();
        } */
        /* if (counter === 8) {
          clearInterval(interval);
          observer.error('Opps!');
        } */
      }, 1000);
    }).pipe(
      map((data: any) => data.value),
      filter((data, idx) => {
        console.log(data, idx);
        return data % 2 === 0;
      })
    );
  }

}
