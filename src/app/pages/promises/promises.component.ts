import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promises',
  templateUrl: './promises.component.html',
  styles: [
  ],
})
export class PromisesComponent implements OnInit {

  constructor() {
    this.counterPromise().then(() => {
      console.log('End Promise');
    })
      .catch(error => console.error(error));
  }

  ngOnInit(): void {
  }

  counterPromise(): Promise<string> {
    return new Promise((resolve, reject) => {
      let counter = 0;
      const st = setInterval(() => {
        counter += 1;
        console.log('counter', counter);
        if (counter === 3) {
          clearInterval(st);
          resolve('OK');
        } else {
          clearInterval(st);
          reject('Managed Error');
        }
      }, 1000);
    });
  }

}
