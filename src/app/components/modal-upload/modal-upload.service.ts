import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalUploadService {

  public type: string;
  public id: string;
  public image: string;

  public notificacion = new EventEmitter<any>();

  constructor() { }

  hideModal() {
    this.type = null;
    this.id = null;
  }

  showModal(type: string, id: string, image: string) {
    this.type = type;
    this.id = id;
    this.image = image;
  }

}
