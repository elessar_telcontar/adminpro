import { Component, OnInit } from '@angular/core';

import Swal from 'sweetalert2';

import { UserService } from '@services/user/user.service';
import { UploadFileService } from '@services/upload-file.service';
import { ModalUploadService } from './modal-upload.service';

@Component({
  selector: 'app-modal-upload',
  templateUrl: './modal-upload.component.html',
  styles: [
  ],
})
export class ModalUploadComponent implements OnInit {

  imagenSubir: File;
  imagenTemporal: string;

  constructor(
    public userService: UserService,
    public uploadFileService: UploadFileService,
    public modalUploadService: ModalUploadService
  ) { }

  ngOnInit(): void {
  }

  seleccionImagen(archivo: File) {
    console.log(archivo);
    if (!archivo) return this.imagenSubir = archivo;
    if (archivo.type.indexOf('image') < 0) {
      Swal.fire({ title: 'El archivo seleccionado no es una imagen', icon: 'error' });
      return this.imagenSubir = null;
    }
    this.imagenSubir = archivo;

    const reader = new FileReader();
    // let urlImgTemp = reader.readAsDataURL(archivo);
    reader.readAsDataURL(archivo);
    reader.onloadend = () => this.imagenTemporal = reader.result.toString();
  }

  closeModal() {
    this.modalUploadService.hideModal();
    this.imagenSubir = null;
    this.imagenTemporal = null;
    console.log(this.imagenSubir, this.imagenTemporal);
  }

  uploadImagen() {
    this.uploadFileService.uploadFile(this.imagenSubir, this.modalUploadService.type, this.modalUploadService.id)
      .then(resp => {
        console.log(resp);
        this.modalUploadService.notificacion.emit(resp);
      })
      .catch(error => {
        console.log('Error en la carga');
      });
  }

}
