import {
  Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef
} from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: [
  ],
})
export class IncrementadorComponent implements OnInit {

  @Input() leyenda = 'Leyenda';
  @Input() percent = 50;

  @Output() changeValue: EventEmitter<number> = new EventEmitter();

  @ViewChild('incrementInput') incrementInput: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  changeProgress(value: number) {
    if ((this.percent <= 0 && value < 0)) {
      return this.percent = 0;
    }
    if ((this.percent >= 100 && value > 0)) {
      return this.percent = 100;
    }
    this.percent += value;
    this.changeValue.emit(this.percent);
    this.incrementInput.nativeElement.focus();
  }

  onChange(newValue: number) {
    if (newValue >= 100) this.percent = 100;
    else if (newValue <= 0) this.percent = 0;
    else this.percent = newValue;
    this.incrementInput.nativeElement.value = this.percent;
    this.changeValue.emit(this.percent);
  }

}
