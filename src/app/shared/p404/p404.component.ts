import { Component, OnInit } from '@angular/core';

declare function init_plugins();

@Component({
  selector: 'app-p404',
  templateUrl: './p404.component.html',
  styleUrls: ['./p404.component.css']
})
export class P404Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
    init_plugins();
  }

}
