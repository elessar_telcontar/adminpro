import { Component, OnInit } from '@angular/core';

import { SidebarService } from '@services/sidebar.service';
import { UserService } from '@services/user/user.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class SidebarComponent implements OnInit {

  constructor(
    public _sidebar: SidebarService,
    public userService: UserService
  ) { }

  ngOnInit(): void {
  }

}
